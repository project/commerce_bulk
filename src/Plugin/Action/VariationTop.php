<?php

namespace Drupal\commerce_bulk\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Move variations to the top of the list.
 *
 * @Action(
 *   id = "commerce_bulk_variation_top",
 *   label = @Translation("Move to top"),
 *   type = "commerce_product_variation"
 * )
 */
class VariationTop extends ActionBase {

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $variations) {
    foreach ($variations as $index => $variation) {
      $variations[$variation->id()] = $variation;
      unset($variations[$index]);
    }
    $product = $variation->getProduct();
    foreach ($product->getVariations() as $variation) {
      $id = $variation->id();
      if (!isset($variations[$id])) {
        $variations[$id] = $variation;
      }
    }
    $product->setVariations($variations);
    $product->save();
  }

  /**
   * {@inheritdoc}
   */
  public function execute($variation = NULL) {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function access($variation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = $variation->access('update', $account, TRUE);

    return $return_as_object ? $result : $result->isAllowed();
  }

}
