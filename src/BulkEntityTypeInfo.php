<?php

namespace Drupal\commerce_bulk;

use Drupal\Core\Entity\EntityInterface;
use Drupal\commerce_product\Entity\ProductAttributeInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\taxonomy\VocabularyInterface;

/**
 * Manipulates entity type information.
 *
 * This class contains primarily bridged hooks for compile-time or
 * cache-clear-time hooks. Runtime hooks should be placed in EntityOperations.
 */
class BulkEntityTypeInfo implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new self();
    $instance->currentUser = $container->get('current_user');

    return $instance;
  }

  /**
   * Adds commerce_bulk operations on an entity that supports it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity on which to define an operation.
   *
   * @return array
   *   An array of operation definitions.
   *
   * @see hook_entity_operation()
   */
  public function entityOperation(EntityInterface $entity) {
    $operations = [];
    if ($entity instanceof ProductAttributeInterface && $this->currentUser->hasPermission("administer commerce_product_attribute")) {
      $url = $entity->toUrl();
      $route = 'view.commerce_bulk_attributes.attribute_page';
      $route_parameters = $url->getRouteParameters();
      $options = $url->getOptions();
      $operations['commerce_bulk_operations'] = [
        'title' => $this->t('Bulk'),
        'weight' => -100,
        'url' => $url->fromRoute($route, $route_parameters, $options),
      ];
    }
    elseif ($entity instanceof VocabularyInterface && $this->currentUser->hasPermission("administer taxonomy")) {
      $url = $entity->toUrl();
      $route = 'view.commerce_bulk_taxonomy.vocabulary_page';
      $route_parameters = $url->getRouteParameters();
      $options = $url->getOptions();
      $operations['commerce_bulk_operations'] = [
        'title' => $this->t('Bulk'),
        'weight' => -100,
        'url' => $url->fromRoute($route, $route_parameters, $options),
      ];
    }

    return $operations;
  }

}
